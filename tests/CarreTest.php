<?php 
use Dev\DemoPhpUnit\Carre;
use PHPUnit\Framework\TestCase;
class CarreTest extends TestCase{
    public function testSurface(){
        $objet = new Carre(10);
        $this->assertEquals(100, $objet->surface());
    }

    public function testSetCote(){
        $this->expectException(Exception::class);
        $objet = new Carre(-23);
    }
/**
* @dataProvider dataForTestSurface
*/
    public function testSurface1($cote, $resultatAttendu){
        $object = new Carre($cote);
        $this->assertEquals($resultatAttendu, $object->surface());
    }
    public static function dataForTestSurface(){
        return [
            [0, 0],
            [10, 100],
            [5, 25]
        ];
}

}
